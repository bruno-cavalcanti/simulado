import os, os.path

listaPerguntas = []
listaRespostas = []

countLine = 0
def arquivosDir(path):
    global countLine
    for _, _, arquivo in os.walk(path):
        if (path == 'perguntas'):
            countLine = len(arquivo)
            listaPerguntas.append(arquivo)
        elif (path == 'respostas'):
            listaRespostas.append(arquivo)

arquivosDir('perguntas')
arquivosDir('respostas')

contador = 0
while (contador < countLine):
    os.system('cls' if os.name == 'nt' else 'clear')
    pergunta = open('perguntas/%s' % listaPerguntas[0][contador], 'r')
    perguntaTexto = pergunta.read()
    print (perguntaTexto)

    opcao = input("--------------------------- : ")

    resposta = open('respostas/%s' % listaRespostas[0][contador], 'r')
    respostaTexto = resposta.read()
    print (respostaTexto)
    resposta.close()
    next = input("Press enter to the next Question...")

    contador += 1
